@extends('layouts.main')
@section('content')
<div class="card">
    <div class="card-header card-header-primary">
      <h4 class="card-title">FORMULAIRE POUR LES PAYS</h4>
      <p class="card-category">Completer le formulaire</p>
    </div>
    <div class="card-body">
      <form method="POST" action="{{route('pays.store')}}">
        @method("POST")
        @csrf
        <div class="row">
          <div class="col-md-3">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">Libelle</label>
              <input type="text" class="form-control" name="libelle" >
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">Description</label>
              <input type="text" class="form-control" name="description" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">code indicatif</label>
              <input type="number" class="form-control" name="code_indicatif" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">continent</label>
              <input type="text" class="form-control" name="continent" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">population</label>
              <input type="number" class="form-control" name="population" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">capitale</label>
              <input type="text" class="form-control" name="capitale">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group bmd-form-group">
              <select class="form-control" name="monnaie">
                <option selected>Choisissez la monnaie</option>
                <option value="1">XOF</option>
                <option value="2">EUR</option>
                <option value="3">DOLLAR</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group bmd-form-group">
                <select class="form-control" name="langue" required>
                    <option selected>Choisissez la langue</option>
                    <option value="1">FR</option>
                    <option value="2">EN</option>
                    <option value="3">AR</option>
                    <option value="3">ES</option>
                </select>
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group bmd-form-group">
              <label class="bmd-label-floating">Superficie</label>
              <input type="number" class="form-control" name="superficie" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group bmd-form-group">
              <select class="form-control" name="est_laique" required>
                <option selected>Pays Laïque ?</option>
                <option value="1">OUI</option>
                <option value="0">NON</option>
              </select>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary pull-right">Ajouter dans le tableau</button>
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
@endsection
