<?php

namespace App\Http\Controllers;

use App\Models\Pay;
use Illuminate\Http\Request;

class PayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("index");
    }
    public function main(){
        return view("layouts.index",[
            "pays"=>Pay::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //dd($request);// die and dump
        //validation de la requete
        $request->validate([
            "libelle" => "required",
            "capitale" => "required",
            "monnaie" => "required",

        ]);
        //enregistrement dans la bd
        $pay = Pay::create([
            "libelle" => $request->get('libelle'),
            "code_indicatif" => $request->get('code_indicatif'),
            "capitale" => $request->get('capitale'),
            "langue" => $request->get('langue'),
            "description" => $request->get('description'),
            "continent" => $request->get('continent'),
            "population" => $request->get('population'),
            "superficie" => $request->get('superficie'),
            "monnaie" => $request->get('monnaie'),
            "est_laique" => $request->get('est_laique'),


        ]);
        return redirect()->route("pay.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function show(Pay $pay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function edit(Pay $pay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pay $pay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pay $pay)
    {
        //
    }
}
