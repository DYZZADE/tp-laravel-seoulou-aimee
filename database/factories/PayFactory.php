<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle'=>$this->faker->title,
            'description'=>$this->faker->sentence,
            'code_indicatif'=>$this->faker->sentence,
            'continent'=>$this->faker->sentence,
            'langue'=>$this->faker->sentence,
            'population'=>$this->faker->randomElement([12333,1234,76543,23456]),
            'capitale'=>$this->faker->title,
            'monnaie'=>$this->faker->title,
            'superficie'=>$this->faker->randomElement([12333,1234,76543,23456]),
            'est_laique'=>$this->faker->randomElement([true,false])

        ];
    }
}
